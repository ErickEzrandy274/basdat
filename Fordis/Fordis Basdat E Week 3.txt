Selamat sore Bu Iis dan teman-teman semua, izinkan saya untuk menjawab diskusi pekan ini.

1. Kontingen suatu cabang olahraga sudah pasti terdiri dari atlet dan official. Suatu atlet tidak dapat berperan sebagai official, begitu juga sebaliknya
constraint disjointness dan completeness = disjoint total 
Hal tersebut karena ada kata sudah pasti yang berarti minimal ada 1 dan terdapat kata tidak dapat berperan sebagai official dan sebaliknya.

2. Dalam aplikasi marketplace, terdapat dua jenis pengguna yaitu penjual dan pembeli. Seorang penjual dapat berperan sebagai pembeli, begitu juga sebaliknya. Selain dua jenis pengguna tersebut, terdapat jenis pengguna lain tanpa peran spesifik.
constraint disjointness dan completeness = overlap partial
Hal tersebut karena ada kata seorang penjual dapat berperan sebagai pembeli dan sebaliknya serta terdapat jenis pengguna lain tanpa peran spesifik

3. Semua anggota dari entitas X merupakan anggota dari entitas Y atau Z. Mungkin saja ada anggota dari Y atau Z yang tidak terdapat dalam X.
constraint disjointness dan completeness = overlap partial
Hal tersebut karena anggota entitas X bisa berada di Y atau bisa berada di Z. Selain itu, terdapat kata "mungkin saja ada ... yg tidak terdapat" yang menandakan partial

Sekian dari jawaban saya, mohon maaf apabila ada kesalahan. Terima kasih!

Salam,
Erick Ezrandy
